import React, { useState } from "react";
import { FormGroup, Label, Input, Button } from "reactstrap";

const Info = (props) => {
  const initialState = { name: "", age: "", address: "" };
  const [newInfo, setNewInfo] = useState(initialState);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewInfo({ ...newInfo, [name]: value });
  };

  const handleAddInfo = (e) => {
    e.preventDefault();
    if (!newInfo.name || !newInfo.age || !newInfo.address)
      return alert("Complete the Information");
    props.addInfo(newInfo);
    setNewInfo(initialState);
  };

  console.log("this is newInfo", newInfo);

  return (
    <React.Fragment>
      <FormGroup
        style={{
          backgroundColor: "#56ccf2",
        }}
      >
        <Label>Name:</Label>
        <Input
          placeholder="Enter your name"
          type="text"
          name="name"
          value={newInfo.name}
          onChange={handleChange}
        />
        <Label>Age:</Label>
        <Input
          placeholder="Enter your age"
          type="number"
          name="age"
          value={newInfo.age}
          onChange={handleChange}
        />
        <Label>Address:</Label>
        <Input
          placeholder="Enter your address"
          type="text"
          name="address"
          value={newInfo.address}
          onChange={handleChange}
        />
      </FormGroup>
      <Button
        className="btn btn-info"
        style={{ marginRight: "20px" }}
        onClick={handleAddInfo}
      >
        Submit
      </Button>
    </React.Fragment>
  );
};
export default Info;
