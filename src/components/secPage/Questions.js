import React, { useState } from "react";

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

const Questions = props => {
  const [dropdownIsOpen, setDropdownIsOpen] = useState(false);
  return (
    <React.Fragment>
      {props.surveys.map((survey, index) => (
        <tr key={index}>
          <td>
            <p>
              Question #{index + 1}
              {": "}
              <span className="text-danger">{survey.question}</span>{" "}
            </p>
          </td>
          <td>
            {/* <Dropdown
              isOpen={dropdownIsOpen}
              toggle={() => setDropdownIsOpen(!dropdownIsOpen)}
            >
              <DropdownToggle caret>
                {!props.answer ? "Answer" : props.answer}
              </DropdownToggle>
              <DropdownMenu>
                {props.answers.map((answer, index) => (
                  <DropdownItem
                    key={index}
                    onClick={() => props.onClick(answer)}
                  >
                    {answer}
                  </DropdownItem>
                ))}
              </DropdownMenu>
            </Dropdown> */}
            <div className=" d-flex py-3 ">
              <div className="custom-control custom-radio mx-3">
                <input
                  type="radio"
                  className="custom-control-input"
                  id="defaultUnchecked"
                  name="defaultExampleRadios"
                />
                <label className="custom-control-label" for="defaultUnchecked">
                  YES
                </label>
              </div>

              <div className="custom-control custom-radio">
                <input
                  type="radio"
                  className="custom-control-input"
                  id="defaultChecked"
                  name="defaultExampleRadios"
                />
                <label className="custom-control-label" for="defaultChecked">
                  NO
                </label>
              </div>
            </div>
          </td>
        </tr>
      ))}
    </React.Fragment>
  );
};
export default Questions;
