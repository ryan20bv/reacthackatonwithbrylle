import React, { useState } from "react";
import Navbar from "../firstPage/Navbar";
import Questions from "./Questions";
import Info from "./Info";
import "../../../src/App.css";
import { Card, CardHeader, CardBody, Table } from "reactstrap";

const Survey = (props) => {
  return (
    <React.Fragment>
      <div className=" d-flex py-5 justify-content-center align-items-center ">
        <div className="col-lg-4 mt-2">
          <h1 className="text-center " style={{ backgroundColor: "#f2994a" }}>
            Information
          </h1>
          <Info informations={props.informations} addInfo={props.addInfo} />
        </div>
        <div className="col-lg-8 mt-2">
          <Card>
            <h1 className="text-center " style={{ backgroundColor: "#ede574" }}>
              Survey Questions
            </h1>

            <CardBody style={{ backgroundColor: "#d3cce3" }}>
              <Table>
                {/* <thead>
                  <tr>
                    <td></td>
                    <td></td>
                  </tr>
                </thead> */}
                <tbody>
                  <Questions surveys={props.surveys} answers={props.answers} />
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Survey;
