import React from "react";
import "../../../src/App.css";

const Navbar = props => {
  return (
    <React.Fragment>
      <nav
        className="navbar navbar-light bg-info d-flex justify-content-center align-items-center header"
        id="nav"
      >
        <h3 className="title">Stop the Spread</h3>
      </nav>
    </React.Fragment>
  );
};
export default Navbar;
