import React, { useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";

const EditForm = (props) => {
  const [name, setName] = useState("");
  const [age, setAge] = useState(null);
  const [address, setAddress] = useState("");

  const handleUpdateInfo = (e) => {
    e.preventDefault();
    props.updateInfo(name, age, address);
    setName("");
    setAge(null);
    setAddress("");
  };

  return (
    <Modal isOpen={props.showForm} toggle={props.toggleForm}>
      <ModalHeader toggle={props.toggleForm}>Edit Information</ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label>Name:</Label>
          <Input
            name="name"
            type="text"
            defaultValue={props.infoToEdit.name}
            onChange={(e) => setName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label>Age:</Label>
          <Input
            name="age"
            type="number"
            defaultValue={props.infoToEdit.age}
            onChange={(e) => setAge(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label>Address:</Label>
          <Input
            name="name"
            type="text"
            defaultValue={props.infoToEdit.address}
            onChange={(e) => setAddress(e.target.value)}
          />
        </FormGroup>
        <Button color="success" onClick={handleUpdateInfo}>
          Update Info
        </Button>
      </ModalBody>
    </Modal>
  );
};
export default EditForm;
