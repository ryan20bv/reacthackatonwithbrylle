import React from "react";
import Navbar from "../firstPage/Navbar";
import { Card, CardHeader, CardBody, Table, Button } from "reactstrap";

const Tabulation = (props) => {
  return (
    <React.Fragment>
      <Card className="col-lg-10 offset-lg-1 mt-2 ">
        <h1 className="text-center " style={{ backgroundColor: "#d3cce3" }}>
          DATA
        </h1>

        <CardBody>
          <Table>
            <thead>
              <tr>
                <td>Name</td>
                <td>Age</td>
                <td>Adress</td>
                <td> Q # 1</td>
                <td> Q # 2</td>
                <td> Q # 3</td>
                <td> Q # 4</td>
                <td> Q # 5</td>
                <td> Q # 6</td>

                <td> </td>
              </tr>
            </thead>
            <tbody>
              {props.informations.map((info, index) => (
                <tr key={index}>
                  <td>{info.name} </td>
                  <td>{info.age} </td>
                  <td>{info.address}</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>
                    <Button
                      color="info"
                      className="mx-2 btn-sm"
                      onClick={() => props.editInfo(info, index)}
                    >
                      Edit
                    </Button>
                    <Button
                      color="danger"
                      className="mx-2 btn-sm"
                      onClick={() => props.deleteInfo(index)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </React.Fragment>
  );
};
export default Tabulation;
