import React, { useState } from "react";
// import Navbar from "./components/firstPage/Navbar";
import Survey from "./components/secPage/Survey";
import Tabulation from "./components/thirdPage/Tabulation";
import AppBrylle from "./components/firstPage/AppBrylle";
import EditForm from "./components/thirdPage/EditForm";

const App = (props) => {
  const [informations, setInformations] = useState([
    {
      name: "Martin McGyver",

      age: 28,

      address: "Texas",
    },
    {
      name: "Richard Willington",

      age: 34,

      address: "New York",
    },
  ]);

  const [surveys, setSurveys] = useState([
    {
      question: "Have you travel abroad for the past two weeks?",
    },
    {
      question: "Are you feeling sick?",
    },
    {
      question: "Do you usually go outside?",
    },
    {
      question: "Do you smoke?",
    },
  ]);

  const [answers, setAnswers] = useState(["Yes", "No"]);
  const [showForm, setShowForm] = useState(false);
  const [infoToEdit, setInfoToEdit] = useState({});
  const [infoIndex, setInfoIndex] = useState(null);

  // functions
  const addInfo = (newInfo) => {
    setInformations([...informations, newInfo]);
  };

  const deleteInfo = (index) => {
    console.log("i am deleting", index);
    const newInfo = informations.filter((_, iIndex) => {
      return iIndex !== index;
    });
    setInformations(newInfo);
  };

  const editInfo = (info, index) => {
    setShowForm(true);
    setInfoToEdit(info);
    setInfoIndex(index);
  };
  const toggleForm = () => {
    setShowForm(false);
  };

  const updateInfo = (name, age, address) => {
    let newInfos = [];
    let editedName = name;
    let editedAge = age;
    let editedAddress = address;
    if (name === "") editedName = infoToEdit.name;
    if (age === null) editedAge = infoToEdit.age;
    if (address === "") editedAddress = infoToEdit.address;
    newInfos = informations.map((indivInfo, index) => {
      if (index === infoIndex) {
        return { name: editedName, age: editedAge, address: editedAddress };
      } else {
        return indivInfo;
      }
    });
    setInfoToEdit({});
    setInfoIndex(null);
    setInformations(newInfos);
    setShowForm(false);
  };

  // end of Functions

  return (
    <React.Fragment>
      <div>
        <AppBrylle />
      </div>
      <div className="vh-100 my-5 ">
        <Survey
          surveys={surveys}
          addInfo={addInfo}
          // answers={answers}
          // informations={informations}
        />
      </div>
      <div className="vh-100 py-5">
        <Tabulation
          informations={informations}
          deleteInfo={deleteInfo}
          editInfo={editInfo}
        />
      </div>
      <EditForm
        showForm={showForm}
        toggleForm={toggleForm}
        infoToEdit={infoToEdit}
        updateInfo={updateInfo}
      />
      <footer>
        <p id="source">Source: World Health Organization</p>
      </footer>
    </React.Fragment>
  );
};

export default App;
